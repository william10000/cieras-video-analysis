# R scripts for aggregating cellular contractility tests #

### What is this repository for? ###

* This scripts generates plots to visualize contractility of cardiac cells cultured under different conditions.

### How do I get set up? ###

* input is a CSV file with the following columns (or any 3 readouts with 2 experimental factors)
      * file name encoded with experimental conditions
      * fractional shortening as %
      * cell aspect ratio
      * cell area in pixels

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* william.wan@colorado.edu