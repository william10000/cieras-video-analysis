# This script runs other scripts to load, pre-process and calculate summary statistics

# written by William Wan (2015-03-05)
# modified on 2015-03-24 for final data
# 2015-03-24: scale for high speed camera at 40x is 0.1225 um^2/px^2 - data are in px^2
# 2015-03-24: created ssh key using git-bash & added to bitbucket

library(ggplot2)
rm(list = ls())
graphics.off()

source("150304_video_analysis_functions.R")
source("150304_video_analysis_load_files.R")
source("150304_video_analysis_pre-process.R")

cal.40x <- 0.1225 # um^2/px^2 conversion factor for high speed camera, 40x objective on Zeiss Examiner

# remove outliers based on histogram of all data
temp.all <- temp.all[!(temp.all$FS > 0.6), ]
temp.all <- temp.all[!(temp.all$AR > 7.75), ]
temp.all <- temp.all[!(temp.all$Area > 4750), ]
temp.all <- temp.all[!(temp.all$Area < 50), ]

# convert px to microns for area
temp.all$Area <- temp.all$Area * cal.40x

# calculate summary stats
stats.all <- sum_stats(temp.all)
stats.01 <- sum_stats(temp.all[grepl("140925", temp.all$File.Name), ])
stats.02 <- sum_stats(temp.all[grepl("141009", temp.all$File.Name), ])
stats.03 <- sum_stats(temp.all[grepl("141113", temp.all$File.Name), ])
stats.04 <- sum_stats(temp.all[grepl("141023", temp.all$File.Name), ])

# reorders summary stats to make it easier to copy to Prism
stats.all.GP <- stats.all[order(stats.all$modulus, stats.all$size), ]
stats.01.GP <- stats.01[order(stats.01$modulus, stats.01$size), ]
stats.02.GP <- stats.02[order(stats.02$modulus, stats.02$size), ]
stats.03.GP <- stats.03[order(stats.03$modulus, stats.03$size), ]
stats.04.GP <- stats.04[order(stats.04$modulus, stats.04$size), ]

# saves summary stats for import into Prism or excel
write.csv(stats.all.GP, "stats_all.csv")
write.csv(stats.01.GP, "stats_140925.csv")
write.csv(stats.02.GP, "stats_141009.csv")
write.csv(stats.03.GP, "stats_141113.csv")
write.csv(stats.04.GP, "stats_141023.csv")
write.csv(temp.all, 'processed_video_stats_CND.csv')

# line graphs of all data
# pd <- position_dodge(0.1)
#  
# ggplot(stats.all, aes(x = size, y = FS, color = factor(modulus))) + 
#   geom_errorbar(aes(ymin = FS - FS.sem, ymax = FS + FS.sem), width = 0.1, size = 1) +
#   geom_line(aes(group = modulus), size = 1) + ylim(0, 0.25) + ggtitle("All data")
# 
# ggplot(stats.all, aes(x = size, y = AR, color = factor(modulus))) + 
#   geom_errorbar(aes(ymin = AR - AR.sem, ymax = AR + AR.sem), width = 0.1, size = 1) +
#   geom_line(aes(group = modulus), size = 1)  + ylim(1, 5) + ggtitle("All data")
# 
# ggplot(stats.all, aes(x = size, y = Area, color = factor(modulus))) + 
#   geom_errorbar(aes(ymin = Area - Area.sem, ymax = Area + Area.sem), width = 0.1, size = 1) +
#   geom_line(aes(group = modulus), size = 1)  + ylim(0, 400) + ggtitle("All data")

# # 140925 (01)
# ggplot(stats.01, aes(x = size, y = FS, color = factor(modulus))) + 
#   geom_errorbar(aes(ymin = FS - FS.sem, ymax = FS + FS.sem), width = 0.1, size = 1) +
#   geom_line(aes(group = modulus), size = 1) + ggtitle("140925")
# 
# ggplot(stats.01, aes(x = size, y = AR, color = factor(modulus))) + 
#   geom_errorbar(aes(ymin = AR - AR.sem, ymax = AR + AR.sem), width = 0.1, size = 1) +
#   geom_line(aes(group = modulus), size = 1) + ggtitle("140925")
# 
# ggplot(stats.01, aes(x = size, y = Area, color = factor(modulus))) + 
#   geom_errorbar(aes(ymin = Area - Area.sem, ymax = Area + Area.sem), width = 0.1, size = 1) +
#   geom_line(aes(group = modulus), size = 1) + ggtitle("140925")
# 
# # 141009 (02)
# ggplot(stats.02, aes(x = size, y = FS, color = factor(modulus))) + 
#   geom_errorbar(aes(ymin = FS - FS.sem, ymax = FS + FS.sem), width = 0.1, size = 1) +
#   geom_line(aes(group = modulus), size = 1) + ggtitle("141009")
# 
# ggplot(stats.02, aes(x = size, y = AR, color = factor(modulus))) + 
#   geom_errorbar(aes(ymin = AR - AR.sem, ymax = AR + AR.sem), width = 0.1, size = 1) +
#   geom_line(aes(group = modulus), size = 1) + ggtitle("141009")
# 
# ggplot(stats.02, aes(x = size, y = Area, color = factor(modulus))) + 
#   geom_errorbar(aes(ymin = Area - Area.sem, ymax = Area + Area.sem), width = 0.1, size = 1) +
#   geom_line(aes(group = modulus), size = 1) + ggtitle("141009")
# 
# # 141113 (03)
# ggplot(stats.03, aes(x = size, y = FS, color = factor(modulus))) + 
#   geom_errorbar(aes(ymin = FS - FS.sem, ymax = FS + FS.sem), width = 0.1, size = 1) +
#   geom_line(aes(group = modulus), size = 1) + ggtitle("141113")
# 
# ggplot(stats.03, aes(x = size, y = AR, color = factor(modulus))) + 
#   geom_errorbar(aes(ymin = AR - AR.sem, ymax = AR + AR.sem), width = 0.1, size = 1) +
#   geom_line(aes(group = modulus), size = 1) + ggtitle("141113")
# 
# ggplot(stats.03, aes(x = size, y = Area, color = factor(modulus))) + 
#   geom_errorbar(aes(ymin = Area - Area.sem, ymax = Area + Area.sem), width = 0.1, size = 1) +
#   geom_line(aes(group = modulus), size = 1) + ggtitle("141113")

# 141023 (04) data set that I analyzed myself
# ggplot(stats.04, aes(x = size, y = FS, color = factor(modulus))) + 
#   geom_errorbar(aes(ymin = FS - FS.sem, ymax = FS + FS.sem), width = 0.1, size = 1) +
#   geom_line(aes(group = modulus), size = 1) + ggtitle("141023")
# 
# ggplot(stats.04, aes(x = size, y = AR, color = factor(modulus))) + 
#   geom_errorbar(aes(ymin = AR - AR.sem, ymax = AR + AR.sem), width = 0.1, size = 1) +
#   geom_line(aes(group = modulus), size = 1) + ggtitle("141023")
# 
# ggplot(na.omit(stats.04), aes(x = size, y = Area, color = factor(modulus))) + 
#   geom_errorbar(aes(ymin = Area - Area.sem, ymax = Area + Area.sem), width = 0.1, size = 1) +
#   geom_line(aes(group = modulus), size = 1) + ggtitle("141023")

# bar graphs
# qplot(size, FS, data = stats.all, geom = "bar", fill = factor(modulus), stat = "identity", position = position_dodge(0.9)) +
#   geom_errorbar(aes(ymin = FS - FS.sem, ymax = FS + FS.sem), position = position_dodge(0.9), width = 0.5) + ylab("Fractional shortening")

# qplot(size, AR, data = stats.all, geom = "bar", fill = factor(modulus), stat = "identity", position = position_dodge(0.9)) +
#   geom_errorbar(aes(ymin = AR - AR.sem, ymax = AR + AR.sem), position = position_dodge(0.9), width = 0.5)
# 
# qplot(size, Area, data = stats.all, geom = "bar", fill = factor(modulus), stat = "identity", position = position_dodge(0.9)) +
#   geom_errorbar(aes(ymin = Area - Area.sem, ymax = Area + Area.sem), position = position_dodge(0.9))
